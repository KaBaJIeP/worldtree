﻿using System;
using System.Data;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WorldTree.Common.UoW.Api;

namespace WorldTree.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true)]
    public class UnitOfWorkActionFilterAttribute : ActionFilterAttribute
    {
        private IUnitOfWork unitOfWork;

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var requestScope = actionContext.Request.GetDependencyScope();

            var factory = requestScope.GetService(typeof(IUnitOfWorkFactory)) as IUnitOfWorkFactory;
            if (factory != null)
            {
                unitOfWork = factory.Create(IsolationLevel.ReadCommitted);
            }

            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (unitOfWork != null)
            {
                if (actionExecutedContext.Exception == null)
                {
                    unitOfWork.Commit();
                }
                else
                {
                    unitOfWork.RollBack();
                }

                unitOfWork.Dispose();
            }

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}