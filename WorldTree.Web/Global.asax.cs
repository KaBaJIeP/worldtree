﻿using System;
using System.Configuration;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;

namespace WorldTree.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            InitAutofac();

            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        }

        private void InitAutofac()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            builder.RegisterAssemblyModules(assemblies);

            RegisterCustomTypes(builder);

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private void RegisterCustomTypes(ContainerBuilder builder)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["WorldTree.MsSql"].ConnectionString;

            builder.Register(x => new SqlTransactionProvider(connectionString))
                   .As<ISqlTransactionProvider>()
                   .InstancePerRequest();

            var mongoConnectionString = ConfigurationManager.ConnectionStrings["WorldTree.MongoDb"].ConnectionString;

            builder.Register(x => new MongoDataAccessConfig(mongoConnectionString)).As<IMongoDataAccessConfig>();

            var hostName = ConfigurationManager.AppSettings["RpcConfig.HostName"];
            var queueName = ConfigurationManager.AppSettings["RpcConfig.QueueName"];
            var routingKey = ConfigurationManager.AppSettings["RpcConfig.RoutingKey"];

            builder.Register(x => new RpcConfig(hostName, queueName, routingKey)).As<IRpcConfig>();
        }
    }
}
