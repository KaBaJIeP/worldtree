﻿using System.Collections.Generic;
using System.Web.Http;
using WorldTree.Main.Query.TreeViewItem.Query;
using WorldTree.Main.Repository.Mongo.Model;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Web.Controllers
{
    public class TreeViewController : ApiController
    {
        private IQueryExecuter queryExecuter;
        public TreeViewController(IQueryExecuter queryExecuter)
        {
            this.queryExecuter = queryExecuter;
        }

        // yes! like-GET request by POST action, we win in less limitations
        [HttpPost]
        public IEnumerable<TreeViewItem> SearchTreeViewItem(SearchTreeViewItem query)
        {
            return queryExecuter.Execute<TreeViewItem>().With(query);
        }
    }
}
