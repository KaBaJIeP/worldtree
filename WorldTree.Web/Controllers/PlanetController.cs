﻿using System.Web.Http;
using WorldTree.Main.Command.Planet.Command;
using WorldTree.Web.BaseControllers;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Web.Controllers
{
    public class PlanetController : SqlCommandController
    {
        private ICommandExecutor commandExecuter;
        public PlanetController(ICommandExecutor commandExecuter)
        {
            this.commandExecuter = commandExecuter;
        }

        [HttpPost]
        public void CreatePlanet(CreatePlanet command)
        {
            commandExecuter.Execute(command);
        }

        [HttpPut]
        public void UpdatePlanet(UpdatePlanet command)
        {
            commandExecuter.Execute(command);
        }

        [HttpDelete]
        public void DeletePlanet(DeletePlanet command)
        {
            commandExecuter.Execute(command);
        }
    }
}
