﻿using System.Web.Http;
using WorldTree.Web.BaseControllers;
using WorldTree.Common.CQRS.Api;
using WorldTree.Main.Command.Continent.Command;

namespace WorldTree.Web.Controllers
{
    public class ContinentController : SqlCommandController
    {
        private ICommandExecutor commandExecuter;

        public ContinentController(ICommandExecutor commandExecuter)
        {
            this.commandExecuter = commandExecuter;
        }

        [HttpPost]
        public void CreateContinent(CreateContinent command)
        {
            commandExecuter.Execute(command);
        }
    }
}
