﻿using System.Web.Http;

namespace WorldTree.Web.Controllers
{
    public class HomeController : ApiController
    {
        [HttpGet]
        public string About()
        { 
            return "Test project for CQRS solution";
        }
    }
}
