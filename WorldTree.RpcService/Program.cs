﻿using System;
using System.ServiceProcess;

namespace WorldTree.RpcService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            var service = new RpcService();
            if (Environment.UserInteractive)
            {
                service.StartInteractive(args);
                Console.WriteLine(@"Press any key to stop program");
                Console.Read();
                service.StopInteractive();
            }
            else
            {
                ServiceBase.Run(service);
            }
        }
    }
}
