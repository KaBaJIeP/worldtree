﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using Autofac;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;
using WorldTree.Common.MQ;

namespace WorldTree.RpcService
{
    public partial class RpcService : ServiceBase
    {
        private IRpcServer rpcServer;
        private Timer syncTimer;
        private static TimeSpan syncSettingsPeriod = TimeSpan.FromSeconds(10);

        public RpcService()
        {
            InitializeComponent();

            var connectionStrings = ConfigurationManager.ConnectionStrings["WorldTree.MongoDb"].ConnectionString;
            var builder = new ContainerBuilder();

            builder.RegisterType<RpcServer>().As<IRpcServer>(); 
            builder.Register(x => new MongoDataAccessConfig(connectionStrings)).As<IMongoDataAccessConfig>();
            builder.RegisterType<MongoDataAccess>().As<IMongoDataAccess>();

            var hostName = ConfigurationManager.AppSettings["RpcConfig.HostName"];
            var queueName = ConfigurationManager.AppSettings["RpcConfig.QueueName"];
            var routingKey = ConfigurationManager.AppSettings["RpcConfig.RoutingKey"];

            builder.Register(x => new RpcConfig(hostName, queueName, routingKey)).As<IRpcConfig>();

            var container = builder.Build();
            rpcServer = container.Resolve<IRpcServer>();

            syncTimer = new Timer(OnCheckService, null, syncSettingsPeriod, syncSettingsPeriod);
        }

        protected override void OnStart(string[] args)
        {
            rpcServer.Start();
        }

        protected override void OnStop()
        {
            if (syncTimer != null)
            {
                syncTimer.Dispose();
            }
                
            rpcServer.Stop();
        }

        public void StartInteractive(string[] args)
        {
            OnStart(args);
        }

        public void StopInteractive()
        {
            OnStop();
        }

        private void OnCheckService(object state)
        {
            // do nothing, just relax
        }
    }
}
