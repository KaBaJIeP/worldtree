﻿using System;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Query.TreeViewItem.Query
{
    public class SearchTreeViewItem : IQuery
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public string Type { get; set; }

        // we can move this properties to separate object for reuse in next search  
        public int Offset { get; set; }
        public int PageSize { get; set; }
    }
}
