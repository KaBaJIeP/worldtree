﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using WorldTree.Main.Query.TreeViewItem.Query;
using WorldTree.Main.Repository.Mongo.Api;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Query.TreeViewItem
{
    public class SearchTreeViewItemQueryHandler : IQueryHandler<SearchTreeViewItem, Repository.Mongo.Model.TreeViewItem>
    {
        private ITreeViewItemRepositoryRead treeViewItemRepositoryRead;
        public SearchTreeViewItemQueryHandler(ITreeViewItemRepositoryRead treeViewItemRepositoryRead)
        {
            this.treeViewItemRepositoryRead = treeViewItemRepositoryRead;
        }
        public IEnumerable<Repository.Mongo.Model.TreeViewItem> Execute(SearchTreeViewItem query)
        {
            Contract.Assume(query.PageSize >= 0);

            var model = new Repository.Mongo.Model.TreeViewItem()
            {
                Id = query.Id,
                Name = query.Name,
                Type = query.Type,
                ParentId = query.ParentId
            };

            return treeViewItemRepositoryRead.Find(model, query.Offset, query.PageSize);
        }
    }
}
