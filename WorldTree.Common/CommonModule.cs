﻿using Autofac;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;
using WorldTree.Common.UoW;
using WorldTree.Common.UoW.Api;
using WorldTree.Common.CQRS;
using WorldTree.Common.CQRS.Api;
using WorldTree.Common.MQ;

namespace WorldTree.Common
{
    public class CommonModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UnitOfWorkFactory>().As<IUnitOfWorkFactory>().InstancePerRequest();
            builder.RegisterType<CommandExecutor>().As<ICommandExecutor>().InstancePerRequest();
            builder.RegisterType<QueryExecuter>().As<IQueryExecuter>().InstancePerRequest();
            builder.RegisterType<SqlDataAccess>().As<ISqlDataAccess>().InstancePerRequest();
            builder.RegisterType<RpcClient>().As<IRpcClient>().InstancePerRequest();
            builder.RegisterType<RpcServer>().As<IRpcServer>();
        }
    }
}
