﻿using System.Collections.Generic;
using WorldTree.Common.MQ;

namespace WorldTree.Common.DataAccess.Api
{
    public interface IMongoDataAccess
    {
        void Insert<T>(T model);
        void Update<T>(T model, FilterCriteria criteria);
        void Delete<T>(T model, FilterCriteria criteria);
        IEnumerable<T> Find<T>(FilterCriteria criteria, int offset, int pageSize);
    }
}
