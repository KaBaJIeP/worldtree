﻿namespace WorldTree.Common.DataAccess.Api
{
    public interface IMongoDataAccessConfig
    {
        string ConnectionString { get; }
    }
}
