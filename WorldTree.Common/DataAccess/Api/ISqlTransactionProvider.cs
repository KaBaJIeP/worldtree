﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace WorldTree.Common.DataAccess.Api
{
    public interface ISqlTransactionProvider: IDisposable
    {
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
        void Rollback();
        void Commit();
        void ExecuteNonQuery(string spName, SqlParameter[] sqlParameters);
        List<T> Execute<T>(string spName, Func<IDataReader, T> reader, SqlParameter[] sqlParameters);
        T SingleOrDefault<T>(string spName, Func<IDataReader, T> reader, SqlParameter[] sqlParameters);
    }
}
