﻿using System;
using System.Data;
using System.Runtime.CompilerServices;

namespace WorldTree.Common.DataAccess.Api
{
    public interface ISqlDataAccess
    {
        void ExecuteNonQuery(object parameters = null, 
            [CallerFilePath] string callerFilePath = "",
            [CallerMemberName] string callerMemberName = "");
        T SingleOrDefault<T>(Func<IDataReader, T> reader, object parameters = null,
                             [CallerFilePath] string callerFilePath = "",
                             [CallerMemberName] string callerMemberName = "");
    }
}
