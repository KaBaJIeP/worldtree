﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics.Contracts;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using WorldTree.Common.DataAccess.Api;

namespace WorldTree.Common.DataAccess
{
    public class SqlTransactionProvider : ISqlTransactionProvider
    {
        private SqlDatabase db;
        private DbConnection dbConnection;
        private DbTransaction dbTransaction;

        public SqlTransactionProvider(string connetionString)
        {
            if(!string.IsNullOrEmpty(connetionString))
                db = new SqlDatabase(connetionString);
        }

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            dbConnection = db.CreateConnection();
            dbConnection.Open();
            dbTransaction = dbConnection.BeginTransaction(isolationLevel);
        }

        public void Rollback()
        {
            if(IsConnectionOpen())
                return;

            if (IsTransactionOk())
            {
                dbTransaction.Rollback();
            }

            CloseConnection();
        }

        public void Commit()
        {
            if(!IsConnectionOpen() || !IsTransactionOk())
                return;

            dbTransaction.Commit();

            CloseConnection();
        }

        protected virtual void Dispose(bool flag)
        {
            CloseConnection();

            if (dbConnection != null)
            {
                dbConnection.Dispose();
                dbConnection = null;
            }

            if (dbTransaction != null)
            {
                dbTransaction.Dispose();
                dbTransaction = null;
            }
            
            db = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void CloseConnection()
        {
            if(IsConnectionOpen())
                dbConnection.Close();
        }

        private bool IsConnectionOpen()
        {
            return dbConnection != null && dbConnection.State == ConnectionState.Open;
        }

        private bool IsTransactionOk()
        {
            return dbTransaction != null && dbTransaction.Connection != null;
        }

        public void ExecuteNonQuery(string spName, SqlParameter[] sqlParameters)
        {
            Contract.Assume(!string.IsNullOrEmpty(spName));

            using (var command = db.GetStoredProcCommand(spName))
            {
                command.Parameters.AddRange(sqlParameters);
                db.ExecuteNonQuery(command, dbTransaction);
            }
        }

        public List<T> Execute<T>(string spName, Func<IDataReader, T> reader, SqlParameter[] sqlParameters)
        {
            Contract.Assume(!string.IsNullOrEmpty(spName));

            List<T> result = new List<T>();
            using (var command = db.GetStoredProcCommand(spName))
            {
                command.Parameters.AddRange(sqlParameters);
                using (var sqlReader = db.ExecuteReader(command, dbTransaction))
                {
                    while (sqlReader.Read())
                    {
                        result.Add(reader(sqlReader));
                    }
                }
            }

            return result;
        }

        public T SingleOrDefault<T>(string spName, Func<IDataReader, T> reader, SqlParameter[] sqlParameters)
        {
            var result = Execute(spName, reader, sqlParameters);
            return result.SingleOrDefault();
        }
    }
}
