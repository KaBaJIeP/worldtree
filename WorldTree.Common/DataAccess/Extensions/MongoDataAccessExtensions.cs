﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using WorldTree.Common.Extensions;
using WorldTree.Common.MQ;

namespace WorldTree.Common.DataAccess.Extensions
{
    public static class MongoDataAccessExtensions
    {
        public static FilterDefinition<BsonDocument> GetFiltersEqualFor(this MongoDataAccess mongo, FilterCriteria criteria)
        {
            List<FilterDefinition<BsonDocument>> filters = new List<FilterDefinition<BsonDocument>>();
            var builder = Builders<BsonDocument>.Filter;

            if (criteria == null)
                return builder.And(filters);

            var data = criteria.Data;

            foreach (var x in data)
            {
                if(x.Value == null)
                    continue;

                if(x.Value.IsEmptyGuid())
                    continue;

                if (string.Equals(x.Name, "Id", StringComparison.OrdinalIgnoreCase) && x.Value.IsGuid())
                {
                    var guid = Guid.Parse(x.Value.ToString());
                    filters.Add(builder.Eq("_id", guid));
                }
                else if (string.Equals(x.Name, "Id", StringComparison.OrdinalIgnoreCase) && !x.Value.IsGuid())
                {
                    filters.Add(builder.Eq("_id", x.Value));           
                }
                else
                {
                    filters.Add(builder.Eq(x.Name, x.Value));
                }
            }

            return builder.And(filters);
        }
    }
}
