﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Contracts;

namespace WorldTree.Common.DataAccess.Extensions
{
    public static class SqlDataAccessExtension
    {
        public static string GetStoredProcedureName(this SqlDataAccess sqlDataAccess, string callerFilePath, string callerMemberName)
        {
            Contract.Assume(!string.IsNullOrEmpty(callerFilePath));
            Contract.Assume(!string.IsNullOrEmpty(callerMemberName));

            int first = callerFilePath.LastIndexOf('\\') + 1;
            int last = callerFilePath.IndexOf(".cs");
            string className = callerFilePath.Substring(first, last - first);

            className = className.Replace("RepositoryRead", string.Empty)
                                 .Replace("RepositoryWrite", string.Empty);

            var spName = string.Format("dbo.{0}{1}", className, callerMemberName);

            return spName;
        }

        public static SqlParameter[] GetSqlParameters(this SqlDataAccess sqlDataAccess, object parameters)
        {
            Contract.Assume(parameters != null);

            var result = new List<SqlParameter>();
            var type = parameters.GetType();
            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                var name = property.Name;
                var value = property.GetValue(parameters);
                var sqlType = GetSqlType(value);
                var sqlParameter = new SqlParameter(name, sqlType)
                {
                    Value = value
                };
                result.Add(sqlParameter);
            }

            return result.ToArray();
        }

        public static SqlDbType GetSqlType(object parameter)
        {
            Contract.Assume(parameter != null);

            if (parameter is Guid)
            {
                return SqlDbType.UniqueIdentifier;
            }

            var type = parameter.GetType();
            
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Int32:
                    return SqlDbType.Int;

                case TypeCode.String:
                    return SqlDbType.NVarChar;

                default:
                    return SqlDbType.NVarChar;
            }
        }
    }
}
