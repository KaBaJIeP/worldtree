﻿using WorldTree.Common.DataAccess.Api;

namespace WorldTree.Common.DataAccess
{
    public class MongoDataAccessConfig : IMongoDataAccessConfig
    {
        private string connection;

        public string ConnectionString
        {
            get { return connection;  }
        }

        public MongoDataAccessConfig(string connectionString )
        {
            this.connection = connectionString;
        }
    }
}
