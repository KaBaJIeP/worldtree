﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.DataAccess.Extensions;
using WorldTree.Common.MQ;

namespace WorldTree.Common.DataAccess
{
    public class MongoDataAccess : IMongoDataAccess
    {
        private IMongoDatabase dataBase;
        public MongoDataAccess(IMongoDataAccessConfig config)
        {
            var mongoUrl = new MongoUrl(config.ConnectionString);

            // important
            var settings = MongoClientSettings.FromUrl(mongoUrl);
            settings.GuidRepresentation = GuidRepresentation.Standard;

            var mongoClient = new MongoClient(settings);
            dataBase = mongoClient.GetDatabase(mongoUrl.DatabaseName);
        }

        public void Insert<T>(T model)
        {
            var entityName = model.GetType().Name;
            var bsonModel = model.ToBsonDocument();
            var collection = dataBase.GetCollection<BsonDocument>(entityName);

            collection.InsertOne(bsonModel);
        }

        public void Update<T>(T model, FilterCriteria criteria)
        {
            var entityName = model.GetType().Name;
            var bsonModel = model.ToBsonDocument();
            var collection = dataBase.GetCollection<BsonDocument>(entityName);
            var filters = this.GetFiltersEqualFor(criteria);

            collection.ReplaceOne(filters, bsonModel);
        }

        public void Delete<T>(T model, FilterCriteria criteria)
        {
            var entityName = model.GetType().Name;
            var collection = dataBase.GetCollection<BsonDocument>(entityName);
            var filters = this.GetFiltersEqualFor(criteria);

            collection.DeleteOne(filters);
        }

        public IEnumerable<T> Find<T>(FilterCriteria criteria, int offset, int pageSize)
        {
            var entityName = typeof(T).Name;
            var collection = dataBase.GetCollection<BsonDocument>(entityName);
            var filters = this.GetFiltersEqualFor(criteria);

            var lazyResult = collection.Find(filters).Skip(offset).Limit(pageSize);

            var result = lazyResult.ToList();
            var typedResult = result.Select(x => BsonSerializer.Deserialize<T>(x));

            return typedResult;
        }
    }
}
