﻿using System;
using System.Data;
using System.Runtime.CompilerServices;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.DataAccess.Extensions;

namespace WorldTree.Common.DataAccess
{
    public class SqlDataAccess : ISqlDataAccess
    {
        private ISqlTransactionProvider sqlTransactionProvider;

        public SqlDataAccess(ISqlTransactionProvider sqlTransactionProvider)
        {
            this.sqlTransactionProvider = sqlTransactionProvider;
        }

        public void ExecuteNonQuery(object parameters = null,
                                    [CallerFilePath] string callerFilePath = "",
                                    [CallerMemberName] string callerMemberName = "")
        {
            var spName = this.GetStoredProcedureName(callerFilePath, callerMemberName);
            var sqlParameters = this.GetSqlParameters(parameters);

            sqlTransactionProvider.ExecuteNonQuery(spName, sqlParameters);
        }

        public T SingleOrDefault<T>(Func<IDataReader, T> reader, object parameters = null,
                                    [CallerFilePath] string callerFilePath = "",
                                    [CallerMemberName] string callerMemberName = "")
        {
            var spName = this.GetStoredProcedureName(callerFilePath, callerMemberName);
            var sqlParameters = this.GetSqlParameters(parameters);

            return sqlTransactionProvider.SingleOrDefault<T>(spName, reader, sqlParameters);
        }

    }
}
