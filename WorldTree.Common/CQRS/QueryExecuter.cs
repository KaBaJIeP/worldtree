﻿using System.Collections.Generic;
using Autofac;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Common.CQRS
{
    public class QueryExecuter : IQueryExecuter
    {
        private IComponentContext context;
        public QueryExecuter(IComponentContext context)
        {
            this.context = context;
        }
        public IQueryWith<TResult> Execute<TResult>()
        {
            return new QueryWith<TResult>(context);
        }

        private class QueryWith<TResult> : IQueryWith<TResult>
        {
            private IComponentContext context;

            public QueryWith(IComponentContext context)
            {
                this.context = context;
            }
            public IEnumerable<TResult> With<TQuery>(TQuery query) where TQuery : IQuery
            {
                var handler = context.Resolve<IQueryHandler<TQuery,TResult>>();
                return handler.Execute(query);
            }
        }
    }
}
