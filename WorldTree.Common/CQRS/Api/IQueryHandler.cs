﻿using System.Collections.Generic;

namespace WorldTree.Common.CQRS.Api
{
    public interface IQueryHandler<in TQuery, out TResult> where TQuery : IQuery
    {
        IEnumerable<TResult> Execute(TQuery query);
    }
}