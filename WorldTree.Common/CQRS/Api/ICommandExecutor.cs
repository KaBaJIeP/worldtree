﻿namespace WorldTree.Common.CQRS.Api
{
    public interface ICommandExecutor
    {
        void Execute<T>(T command) where T : ICommand;
    }
}
