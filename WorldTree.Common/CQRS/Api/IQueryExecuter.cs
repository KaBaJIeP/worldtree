﻿
namespace WorldTree.Common.CQRS.Api
{
    public interface IQueryExecuter
    {
        IQueryWith<TResult> Execute<TResult>();
    }
}
