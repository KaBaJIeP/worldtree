﻿using System.Collections.Generic;

namespace WorldTree.Common.CQRS.Api
{
    public interface IQueryWith<out TResult>
    {
        IEnumerable<TResult> With<TQuery>(TQuery query) where TQuery : IQuery;
    }
}
