﻿namespace WorldTree.Common.CQRS.Api
{
    public interface ICommandHandler<in T> where T : ICommand 
    {
        void Execute(T command);
    }
}
