﻿using Autofac;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Common.CQRS
{
    public class CommandExecutor : ICommandExecutor
    {
        private IComponentContext context;

        public CommandExecutor(IComponentContext context)
        {
            this.context = context;
        }

        public void Execute<T>(T command) where T : ICommand
        {
            var handler = context.Resolve<ICommandHandler<T>>();
            handler.Execute(command);
        }
    }
}
