﻿using System;

namespace WorldTree.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsGuid(this object source)
        {
            if (source == null)
                return false;

            Guid x;
            return Guid.TryParse(source.ToString(), out x);
        }

        public static bool IsEmptyGuid(this object source)
        {
            if (source.IsGuid())
            {
                return Guid.Parse(source.ToString()) == Guid.Empty;
            }

            return false;
        }
    }
}
