﻿using System;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;
using WorldTree.Common.MQ.Extensions;

namespace WorldTree.Common.MQ
{
    public class RpcServer : IRpcServer
    {
        private IConnection connection;
        private IModel channel;
        private EventingBasicConsumer consumer;
        private IMongoDataAccess mongoDataAccess;

        public RpcServer(IMongoDataAccess mongoDataAccess, IRpcConfig config)
        {
            this.mongoDataAccess = mongoDataAccess;

            var factory = new ConnectionFactory() { HostName = config.HostName };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            
            channel.QueueDeclare(queue: config.QueueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
            channel.BasicQos(0, 1, false);
            consumer = new EventingBasicConsumer(channel);
            channel.BasicConsume(queue: config.QueueName, noAck: false, consumer: consumer);
        }
        public void Start()
        {
            consumer.Received += OnReceived;
        }

        public void Stop()
        {
            consumer.Received -= OnReceived;
            Dispose();
        }

        private void OnReceived(object sender, BasicDeliverEventArgs args)
        {
            var response = string.Empty;
            var body = args.Body;
            var props = args.BasicProperties;
            var replyProps = channel.CreateBasicProperties();
            replyProps.CorrelationId = props.CorrelationId;

            var methodType = MethodType.Unknown;
            var criteria = new FilterCriteria();

            if (props.Headers != null)
            {
                methodType = props.Headers.DeserializeByteHeader<MethodType>();
                criteria = props.Headers.DeserializeByteHeader<FilterCriteria>();
            }

            try
            {
                 var typeOfReceivedObject = Type.GetType(props.Type, true);
                var message = Encoding.UTF8.GetString(body);
                var receivedObject = JsonConvert.DeserializeObject(message, typeOfReceivedObject);

                switch (methodType)
                {
                    case MethodType.Insert:
                        mongoDataAccess.Insert(receivedObject);
                        break;
                    case MethodType.Update:
                        mongoDataAccess.Update(receivedObject, criteria);
                        break;
                    case MethodType.Delete:
                        mongoDataAccess.Delete(receivedObject, criteria);
                        break;
                    case MethodType.Unknown:
                        break;
                    default: break;
                }
                
                response = "Response From Server " + Guid.NewGuid() + " -> I Received : " + message;                
            }
            catch (Exception e)
            {
                response = "";
            }
            finally
            {
                var responseBytes = Encoding.UTF8.GetBytes(response);
                channel.BasicPublish(exchange: "", routingKey: props.ReplyTo, basicProperties: replyProps, body: responseBytes);
                channel.BasicAck(deliveryTag: args.DeliveryTag, multiple: false);
            }
        }

        

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool flag)
        {
            if (channel != null)
            {
                channel.Dispose();
                channel = null;
            }

            if (connection != null)
            {
                connection.Dispose();
                connection = null;
            }

            consumer = null;
        }
    }
}
