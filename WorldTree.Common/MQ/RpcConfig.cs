﻿using System.Diagnostics.Contracts;
using WorldTree.Common.MQ.Api;

namespace WorldTree.Common.MQ
{
    public class RpcConfig : IRpcConfig
    {
        private string hostName;
        private string queueName;
        private string routingKey;

        public string HostName
        {
            get { return hostName; }
        }

        public string QueueName
        {
            get { return queueName; }
        }

        public string RoutingKey
        {
            get { return routingKey; }
        }

        public RpcConfig(string hostName, string queueName, string routingKey)
        {
            Contract.Assume(!string.IsNullOrEmpty(hostName));

            this.hostName = hostName;
            this.queueName = queueName;
            this.routingKey = routingKey;
        }
    }
}
