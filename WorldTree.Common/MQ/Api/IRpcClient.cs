﻿using System;

namespace WorldTree.Common.MQ.Api
{
    public interface IRpcClient : IDisposable
    {
        IRpcClient Send<T>(T sendObject, MethodType type, FilterCriteria criteria);
    }
}
