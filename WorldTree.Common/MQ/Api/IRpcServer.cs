﻿using System;

namespace WorldTree.Common.MQ.Api
{
    public interface IRpcServer : IDisposable
    {
        void Start();
        void Stop();
    }
}
