﻿namespace WorldTree.Common.MQ.Api
{
    public interface IRpcConfig
    {
        string HostName { get; }
        string QueueName { get; }
        string RoutingKey { get; }
    }
}
