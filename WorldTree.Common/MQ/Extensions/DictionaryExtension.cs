﻿using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace WorldTree.Common.MQ.Extensions
{
    public static class DictionaryExtension
    {
        public static T DeserializeByteHeader<T>(this IDictionary<string, object> dictionary)
        {
            var result = default(T);
            if (dictionary.ContainsKey(typeof(T).Name))
            {
                var json = Encoding.UTF8.GetString(dictionary[typeof(T).Name] as byte[]);
                result = JsonConvert.DeserializeObject<T>(json);
            }

            return result;
        }
    }
}
