﻿namespace WorldTree.Common.MQ
{
    public enum MethodType
    {
        Unknown = 0,
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
