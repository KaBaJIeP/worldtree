﻿using System;
using System.Diagnostics.Contracts;

namespace WorldTree.Common.MQ
{
    public class Criteria
    {
        private string name;
        private object value;
        private Type type;

        public Type Type
        {
            get { return type; }
        }

        public string Name
        {
            get { return name; }
        }

        public object Value
        {
            get
            {
                return value;
            }
        }

        public Criteria(string name, object value)
        {
            Contract.Assume(!string.IsNullOrEmpty(name));

            this.name = name;
            this.value = value;

            if (value != null)
                this.type = value.GetType();
        }
    }
}
