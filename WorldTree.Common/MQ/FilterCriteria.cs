﻿using System.Collections.Generic;
using System.Linq;

namespace WorldTree.Common.MQ
{
    public class FilterCriteria
    {
        private List<Criteria> criteries = new List<Criteria>();

        public List<Criteria> Data
        {
            get { return this.criteries; }
        }

        public FilterCriteria(params Criteria[] data)
        {
            if (data != null)
            {
                criteries = data.ToList();
            }
        }
    }
}
