﻿using System;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using WorldTree.Common.MQ.Api;

namespace WorldTree.Common.MQ
{
    public class RpcClient : IRpcClient
    {
        private IConnection connection;
        private IModel channel;
        private IRpcConfig config;
        private string replyQueueName;
        private EventingBasicConsumer consumer;
        private AutoResetEvent waitHandle;

        public RpcClient(IRpcConfig config)
        {
            this.config = config;
            var factory = new ConnectionFactory() { HostName = config.HostName };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            replyQueueName = channel.QueueDeclare().QueueName;
            consumer = new EventingBasicConsumer(channel);
            channel.BasicConsume(queue: replyQueueName,
                                 noAck: true,
                                 consumer: consumer);
        }

        public IRpcClient Send<T>(T sendObject, MethodType type, FilterCriteria criteria = null)
        {
            var corrId = Guid.NewGuid().ToString();
            var props = channel.CreateBasicProperties();
            props.ReplyTo = replyQueueName;
            props.CorrelationId = corrId;
            props.Type = typeof(T).AssemblyQualifiedName;

            props.Headers = new ConcurrentDictionary<string, object>();
            props.Headers.Add(typeof(MethodType).Name, JsonConvert.SerializeObject(type));

            if(criteria != null)
                props.Headers.Add(typeof(FilterCriteria).Name, JsonConvert.SerializeObject(criteria));

            var message = JsonConvert.SerializeObject(sendObject);
            var messageBytes = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(exchange: string.Empty,
                                 routingKey: config.RoutingKey,
                                 basicProperties: props,
                                 body: messageBytes);

            waitHandle = new AutoResetEvent(false);

            var result = string.Empty;
            consumer.Received += (o, e) =>
            {
                if (e.BasicProperties.CorrelationId == corrId)
                {
                    result = Encoding.UTF8.GetString(e.Body);
                    waitHandle.Set();
                }
            };

            waitHandle.WaitOne();

            return this;
        }

        public void Dispose(bool flag)
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
                connection = null;
            }

            if (channel != null)
            {
                channel.Dispose();
                channel = null;
            }

            if (waitHandle != null)
            {
                waitHandle.Dispose();
                waitHandle = null;
            }

            consumer = null;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
