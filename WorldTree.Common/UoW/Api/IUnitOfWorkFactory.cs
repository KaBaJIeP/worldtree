﻿using System.Data;

namespace WorldTree.Common.UoW.Api
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create(IsolationLevel isolationLevel);
        IUnitOfWork Create();
    }
}
