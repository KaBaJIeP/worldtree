﻿using System;

namespace WorldTree.Common.UoW.Api
{
    public interface IUnitOfWork: IDisposable
    {
        void Commit();
        void RollBack();
    }
}
