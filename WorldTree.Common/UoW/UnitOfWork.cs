﻿using System;
using System.Data;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.UoW.Api;

namespace WorldTree.Common.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private ISqlTransactionProvider sqlTransactionProvider;

        public UnitOfWork(ISqlTransactionProvider sqlTransactionProvider, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            if (sqlTransactionProvider == null)
                throw new ArgumentNullException(nameof(sqlTransactionProvider));

            this.sqlTransactionProvider = sqlTransactionProvider;
            sqlTransactionProvider.BeginTransaction(isolationLevel);
        }

        public void Commit()
        {
            if (sqlTransactionProvider == null)
            {
                throw new InvalidOperationException("Transation not active");
            }
           
            sqlTransactionProvider.Commit();
        }

        public void RollBack()
        {
            if(sqlTransactionProvider != null)
                sqlTransactionProvider.Rollback();
        }

        protected virtual void Dispose(bool flag)
        {
            if (sqlTransactionProvider != null)
            {
                sqlTransactionProvider.Dispose();
                sqlTransactionProvider = null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
