﻿using System.Data;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.UoW.Api;

namespace WorldTree.Common.UoW
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private ISqlTransactionProvider sqlTransactionProvider;

        public UnitOfWorkFactory(ISqlTransactionProvider sqlTransactionProvider)
        {
            this.sqlTransactionProvider = sqlTransactionProvider;
        }
        public IUnitOfWork Create(IsolationLevel isolationLevel)
        {
           return new UnitOfWork(sqlTransactionProvider, isolationLevel);
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWork(sqlTransactionProvider, isolationLevel: IsolationLevel.ReadCommitted);
        }
    }
}
