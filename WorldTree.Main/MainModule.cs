﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace WorldTree.Main
{
    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var asm = Assembly.GetExecutingAssembly();

            RegistrTypesWhereEndsWith(builder, asm, "RepositoryWrite");
            RegistrTypesWhereEndsWith(builder, asm, "RepositoryRead");
        }

        private void RegistrTypesWhereEndsWith(ContainerBuilder builder, Assembly asm, string suffix)
        {
            builder.RegisterAssemblyTypes(asm)
                   .Where(x => x.Name.EndsWith(suffix))
                   .AsImplementedInterfaces()
                   .InstancePerRequest();
        }
    }
}
