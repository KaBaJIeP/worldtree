﻿using System;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Main.Repository.Sql.Model;
using System.Data;
using WorldTree.Main.Repository.Sql.Api;

namespace WorldTree.Main.Repository.Sql
{
    public class PlanetRepositoryRead : SqlDataAccess, IPlanetRepositoryRead
    {
        public PlanetRepositoryRead(ISqlTransactionProvider sqlTransactionProvider) : base(sqlTransactionProvider)
        { 
        }

        public Planet Get(Guid id)
        {
            var parameters = new {Id = id};
            return base.SingleOrDefault(PlanetReader, parameters);
        }

        // we can replace this with reflection function if needed
        private Planet PlanetReader(IDataReader reader)
        {
            var result = new Planet
            {
                Id = Guid.Parse(reader["Id"].ToString()),
                Name = reader["Name"].ToString()
            };

            return result;
        }
    }
}
