﻿using System;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Main.Repository.Sql.Api;

namespace WorldTree.Main.Repository.Sql
{
    public class ContinentRepositoryWrite : SqlDataAccess, IContinentRepositoryWrite
    {
        public ContinentRepositoryWrite(ISqlTransactionProvider sqlTransactionProvider) : base(sqlTransactionProvider)
        {
        }

        public Guid Create(string name, Guid planetId)
        {
            var parameters = new { Id = Guid.NewGuid(), Name = name, @PlanetId = planetId };
            base.ExecuteNonQuery(parameters);

            return parameters.Id;
        }
    }
}
