﻿using System;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Main.Repository.Sql.Model;
using System.Data;
using WorldTree.Main.Repository.Sql.Api;

namespace WorldTree.Main.Repository.Sql
{
    public class ContinentRepositoryRead : SqlDataAccess, IContinentRepositoryRead
    {
        public ContinentRepositoryRead(ISqlTransactionProvider sqlTransactionProvider) : base(sqlTransactionProvider)
        { 
        }

        public Continent Get(Guid id)
        {
            var parameters = new {Id = id};
            return base.SingleOrDefault(ContinentReader, parameters);
        }

        // we can replace this with reflection function if needed
        private Continent ContinentReader(IDataReader reader)
        {
            var result = new Continent()
            {
                Id = Guid.Parse(reader["Id"].ToString()),
                Name = reader["Name"].ToString(),
                PlanetId = Guid.Parse(reader["PlanetId"].ToString()),
            };

            return result;
        }
    }
}
