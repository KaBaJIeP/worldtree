﻿using System;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Main.Repository.Sql.Api;
using WorldTree.Main.Repository.Sql.Model;

namespace WorldTree.Main.Repository.Sql
{
    public class PlanetRepositoryWrite : SqlDataAccess, IPlanetRepositoryWrite
    {
        public PlanetRepositoryWrite(ISqlTransactionProvider sqlTransactionProvider) : base(sqlTransactionProvider)
        { 
        }

        public Guid Create(string name)
        {
            var parameters = new { Id = Guid.NewGuid(), Name = name };
            base.ExecuteNonQuery(parameters);

            return parameters.Id;
        }

        public void Update(Planet model)
        {
            var parameters = new { Id = model.Id, Name = model.Name };
            base.ExecuteNonQuery(parameters);
        }

        public void Delete(Guid id)
        {
            var parameters = new { Id = id };
            base.ExecuteNonQuery(parameters);
        }
    }
}
