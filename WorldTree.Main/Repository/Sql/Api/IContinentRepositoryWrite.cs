﻿using System;

namespace WorldTree.Main.Repository.Sql.Api
{
    public interface IContinentRepositoryWrite
    {
        Guid Create(string name, Guid planetId);
    }
}
