﻿using System;
using WorldTree.Main.Repository.Sql.Model;

namespace WorldTree.Main.Repository.Sql.Api
{
    public interface IPlanetRepositoryRead
    {
        Planet Get(Guid id);
    }
}
