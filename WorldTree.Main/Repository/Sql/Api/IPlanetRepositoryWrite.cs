﻿using System;
using WorldTree.Main.Repository.Sql.Model;

namespace WorldTree.Main.Repository.Sql.Api
{
    public interface IPlanetRepositoryWrite
    {
        Guid Create(string name);

        void Update(Planet model);

        void Delete(Guid id);
    }
}
