﻿using System;

namespace WorldTree.Main.Repository.Sql.Model
{
    public class Planet
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
