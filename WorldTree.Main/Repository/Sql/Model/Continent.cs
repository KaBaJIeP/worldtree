﻿using System;

namespace WorldTree.Main.Repository.Sql.Model
{
    public class Continent
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid PlanetId { get; set; }
    }
}
