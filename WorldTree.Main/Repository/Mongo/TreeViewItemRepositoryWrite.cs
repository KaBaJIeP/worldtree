﻿using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Main.Repository.Mongo.Api;
using WorldTree.Main.Repository.Mongo.Model;

namespace WorldTree.Main.Repository.Mongo
{
    public class TreeViewItemRepositoryWrite : MongoDataAccess, ITreeViewItemRepositoryWrite
    {
        public TreeViewItemRepositoryWrite(IMongoDataAccessConfig config): base(config)
        {
        }

        public void Create(TreeViewItem model)
        {
            base.Insert(model);
        }
    }
}
