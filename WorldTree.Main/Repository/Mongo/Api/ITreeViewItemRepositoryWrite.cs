﻿using WorldTree.Main.Repository.Mongo.Model;

namespace WorldTree.Main.Repository.Mongo.Api
{
    public interface ITreeViewItemRepositoryWrite
    {
        void Create(TreeViewItem model);
    }
}
