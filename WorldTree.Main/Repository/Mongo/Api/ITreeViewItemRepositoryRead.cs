﻿using System.Collections.Generic;
using WorldTree.Main.Repository.Mongo.Model;

namespace WorldTree.Main.Repository.Mongo.Api
{
    public interface ITreeViewItemRepositoryRead
    {
        IEnumerable<TreeViewItem> Find(TreeViewItem model, int offset, int pageSize);
    }
}
