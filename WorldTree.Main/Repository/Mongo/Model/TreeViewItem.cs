﻿using System;

namespace WorldTree.Main.Repository.Mongo.Model
{
    public class TreeViewItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public string Type { get; set; }

    }
}
