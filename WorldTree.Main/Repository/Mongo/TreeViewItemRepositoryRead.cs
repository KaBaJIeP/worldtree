﻿using System.Collections.Generic;
using WorldTree.Common.DataAccess;
using WorldTree.Common.DataAccess.Api;
using WorldTree.Common.MQ;
using WorldTree.Main.Repository.Mongo.Api;
using WorldTree.Main.Repository.Mongo.Model;

namespace WorldTree.Main.Repository.Mongo
{
    public class TreeViewItemRepositoryRead : MongoDataAccess, ITreeViewItemRepositoryRead
    {
        public TreeViewItemRepositoryRead(IMongoDataAccessConfig config): base(config)
        {
        }

        public IEnumerable<TreeViewItem> Find(TreeViewItem model, int offset, int pageSize)
        {
            var criteria = new FilterCriteria(new Criteria[]
            {
                new Criteria(nameof(model.Id), model.Id),
                new Criteria(nameof(model.Name), model.Name),
                new Criteria(nameof(model.ParentId), model.ParentId),
                new Criteria(nameof(model.Type), model.Type)
            });

            return base.Find<TreeViewItem>(criteria, offset, pageSize);
        }
    }
}
