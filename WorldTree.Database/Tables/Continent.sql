﻿create table dbo.Continent
(
	Id uniqueidentifier not null,
	Name nvarchar(100) not null,
	PlanetId uniqueidentifier not null,
	[RowVersion] timestamp not null, 
    RowVersionLong as (convert(bigint,[RowVersion],0)) persisted,

	constraint PK_Continent primary key (Id),
	constraint FK_Continent_PlanetId foreign key(PlanetId) references dbo.Planet(Id)
)
