﻿create table dbo.Planet
(
	Id uniqueidentifier not null,
	Name nvarchar(100) not null,
	[RowVersion] timestamp not null, 
    RowVersionLong as (convert(bigint,[RowVersion],0)) persisted,

	constraint PK_Planet primary key (Id)
)
