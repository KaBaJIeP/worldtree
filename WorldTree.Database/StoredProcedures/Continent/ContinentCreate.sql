﻿
create procedure dbo.ContinentCreate
	@Id uniqueidentifier,
	@Name nvarchar(100),
	@PlanetId uniqueidentifier
as
begin
	insert into dbo.Continent(Id, Name, PlanetId)
	values(@Id, @Name, @PlanetId)
end