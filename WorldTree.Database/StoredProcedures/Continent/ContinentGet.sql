﻿
create procedure dbo.ContinentGet
	@Id uniqueidentifier
as
begin
	select Id, Name, PlanetId
	from dbo.Continent
	where Id = @Id
end