﻿
create procedure dbo.PlanetGet
	@Id uniqueidentifier
as
begin
	select Id, Name
	from dbo.Planet
	where Id = @Id
end