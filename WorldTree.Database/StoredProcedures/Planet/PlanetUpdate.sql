﻿
create procedure dbo.PlanetUpdate
	@Id uniqueidentifier,
	@Name nvarchar(100)
as
begin
	update dbo.Planet
	set Name = @Name
	where Id = @Id
end
