﻿
create procedure dbo.PlanetCreate
	@Id uniqueidentifier,
	@Name nvarchar(100)
as
begin
	insert into dbo.Planet(Id, Name)
	values(@Id, @Name)
end