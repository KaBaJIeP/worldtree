﻿
create procedure dbo.PlanetDelete
	@Id uniqueidentifier
as
begin
	delete dbo.Planet
	where Id = @Id
end