﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace WorldTree.Main.Command
{
    public class CommandModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var asm = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(asm)
                   .Where(x => x.Name.EndsWith("CommandHandler"))
                   .As(t => t.GetInterfaces()[0])
                   .InstancePerDependency();
        }
    }
}
