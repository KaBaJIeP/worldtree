﻿using WorldTree.Main.Command.TreeViewItem.Command;
using WorldTree.Main.Repository.Mongo.Api;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.TreeViewItem
{
    public class CreateTreeViewItemCommandHandler : ICommandHandler<CreateTreeViewItem>
    {
        private ITreeViewItemRepositoryWrite treeViewItemRepositoryWrite;
        public CreateTreeViewItemCommandHandler(ITreeViewItemRepositoryWrite treeViewItemRepositoryWrite)
        {
            this.treeViewItemRepositoryWrite = treeViewItemRepositoryWrite;
        }

        public void Execute(CreateTreeViewItem command)
        {
            // we can add validation if needed
            
            // we can add auto map feature to dal model
            var model = new Repository.Mongo.Model.TreeViewItem()
            {
                Id = command.Id,
                Name = command.Name,
                ParentId = command.ParentId,
                Type = command.Type
            };

            treeViewItemRepositoryWrite.Create(model);
        }
    }
}
