﻿using System;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.TreeViewItem.Command
{
    public class CreateTreeViewItem : ICommand
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ParentId { get; set; }
        public string Type { get; set; }
    }
}
