﻿using System;
using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;
using WorldTree.Main.Command.Planet.Command;
using WorldTree.Main.Repository.Sql.Api;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.Planet
{
    public class UpdatePlanetCommandHandler : ICommandHandler<UpdatePlanet>
    {
        private IPlanetRepositoryWrite planetRepositoryWrite;
        private IPlanetRepositoryRead planetRepositoryRead;
        private IRpcClient rpcClient;

        public UpdatePlanetCommandHandler(IPlanetRepositoryWrite planetRepositoryWrite,
                                          IPlanetRepositoryRead planetRepositoryRead,
                                          IRpcClient rpcClient)
        {
            this.planetRepositoryWrite = planetRepositoryWrite;
            this.planetRepositoryRead = planetRepositoryRead;
            this.rpcClient = rpcClient;
        }

        public void Execute(UpdatePlanet command)
        {
            var planet = planetRepositoryRead.Get(command.Id);

            if (planet == null)
                throw new ArgumentNullException(nameof(planet));

            planet.Name = command.Name;

            planetRepositoryWrite.Update(planet);
            
            var sendObject = new Repository.Mongo.Model.TreeViewItem()
            {
                Id = planet.Id,
                ParentId = null,
                Name = planet.Name,
                Type = planet.GetType().Name
            };

            var criteria = new FilterCriteria(new Criteria[]
            {
                new Criteria(nameof(sendObject.Id), sendObject.Id),
            });

            using (rpcClient.Send(sendObject, MethodType.Update, criteria)) ;
        }
    }
}
