﻿using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.Planet.Command
{
    public class CreatePlanet : ICommand
    {
        public string Name { get; set; }
    }
}
