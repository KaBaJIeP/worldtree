﻿using System;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.Planet.Command
{
    public class UpdatePlanet : ICommand
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
