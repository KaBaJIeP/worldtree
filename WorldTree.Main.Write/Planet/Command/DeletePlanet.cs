﻿using System;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.Planet.Command
{
    public class DeletePlanet : ICommand
    {
        public Guid Id { get; set; }
    }
}
