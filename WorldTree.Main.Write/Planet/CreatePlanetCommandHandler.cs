﻿using System;
using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;
using WorldTree.Main.Command.Planet.Command;
using WorldTree.Main.Repository.Sql.Api;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.Planet
{
    public class CreatePlanetCommandHandler : ICommandHandler<CreatePlanet>
    {
        private IPlanetRepositoryWrite planetRepositoryWrite;
        private IPlanetRepositoryRead planetRepositoryRead;
        private IRpcClient rpcClient;

        public CreatePlanetCommandHandler(IPlanetRepositoryWrite planetRepositoryWrite,
                                          IPlanetRepositoryRead planetRepositoryRead,
                                          IRpcClient rpcClient)
        {
            this.planetRepositoryWrite = planetRepositoryWrite;
            this.planetRepositoryRead = planetRepositoryRead;
            this.rpcClient = rpcClient;
        }

        public void Execute(CreatePlanet command)
        {
            var id = planetRepositoryWrite.Create(command.Name);
            var planet = planetRepositoryRead.Get(id);

            if (planet == null)
                throw new ArgumentNullException(nameof(planet));

            // we can move this code to auto mapper if needed
            var sendObject = new Repository.Mongo.Model.TreeViewItem()
            {
                Id = planet.Id,
                ParentId = null,
                Name = planet.Name,
                Type = planet.GetType().Name // if entity contract class name changed we need create constants or enum.value to staying consistent
            };

            var criteria = new FilterCriteria(new Criteria[]
            {
                new Criteria(nameof(sendObject.Id), sendObject.Id), 
            });

            using (rpcClient.Send(sendObject, MethodType.Insert, criteria)) ;
        }
    }
}
