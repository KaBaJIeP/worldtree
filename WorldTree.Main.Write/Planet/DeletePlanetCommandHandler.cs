﻿using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;
using WorldTree.Main.Command.Planet.Command;
using WorldTree.Main.Repository.Sql.Api;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.Planet
{
    public class DeletePlanetCommandHandler: ICommandHandler<DeletePlanet>
    {
        private IPlanetRepositoryWrite planetRepositoryWrite;
        private IPlanetRepositoryRead planetRepositoryRead;
        private IRpcClient rpcClient;

        public DeletePlanetCommandHandler(IPlanetRepositoryWrite planetRepositoryWrite,
                                          IPlanetRepositoryRead planetRepositoryRead,
                                          IRpcClient rpcClient)
        {
            this.planetRepositoryWrite = planetRepositoryWrite;
            this.planetRepositoryRead = planetRepositoryRead;
            this.rpcClient = rpcClient;
        }

        public void Execute(DeletePlanet command)
        {
            var planet = planetRepositoryRead.Get(command.Id);

            if(planet == null)
                return;

            planetRepositoryWrite.Delete(command.Id);

            var sendObject = new Repository.Mongo.Model.TreeViewItem()
            {
                Id = command.Id
            };

            var criteria = new FilterCriteria(new Criteria[]
            {
                new Criteria(nameof(sendObject.Id), sendObject.Id),
            });

            using (rpcClient.Send(sendObject, MethodType.Delete, criteria));
        }
    }
}
