﻿using System;
using WorldTree.Common.CQRS.Api;
using WorldTree.Common.MQ;
using WorldTree.Common.MQ.Api;
using WorldTree.Main.Command.Continent.Command;
using WorldTree.Main.Repository.Sql.Api;

namespace WorldTree.Main.Command.Continent
{
    public class CreateContinentCommandHandler : ICommandHandler<CreateContinent>
    {
        private IContinentRepositoryWrite continentRepositoryWrite;
        private IContinentRepositoryRead continentRepositoryRead;
        private IRpcClient rpcClient;

        public CreateContinentCommandHandler(IContinentRepositoryWrite continentRepositoryWrite,
                                             IContinentRepositoryRead continentRepositoryRead,
                                             IRpcClient rpcClient)
        {
            this.continentRepositoryWrite = continentRepositoryWrite;
            this.continentRepositoryRead = continentRepositoryRead;
            this.rpcClient = rpcClient;
        }

        public void Execute(CreateContinent command)
        {
            var id = continentRepositoryWrite.Create(command.Name, command.ParentId);
            var continent = continentRepositoryRead.Get(id);

            if (continent == null)
                throw new ArgumentNullException(nameof(continent));

            // we can move this code to auto mapper if needed
            var sendObject = new Repository.Mongo.Model.TreeViewItem()
            {
                Id = continent.Id,
                ParentId = continent.PlanetId,
                Name = continent.Name,
                Type = continent.GetType().Name // if entity contract class name changed we need create constants or enum.value to staying consistent
            };

            var criteria = new FilterCriteria(new Criteria[]
            {
                new Criteria(nameof(sendObject.Id), sendObject.Id),
            });

            using (rpcClient.Send(sendObject, MethodType.Insert, criteria)) ;
        }
    }
}
