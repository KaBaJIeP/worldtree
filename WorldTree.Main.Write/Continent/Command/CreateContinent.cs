﻿using System;
using WorldTree.Common.CQRS.Api;

namespace WorldTree.Main.Command.Continent.Command
{
    public class CreateContinent : ICommand
    {
        public string Name { get; set; }
        public Guid ParentId { get; set; }
    }
}
